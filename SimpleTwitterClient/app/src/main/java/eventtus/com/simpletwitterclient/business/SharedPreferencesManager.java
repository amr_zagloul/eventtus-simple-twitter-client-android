package eventtus.com.simpletwitterclient.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by amrzagloul on 10/1/16.
 */

public class SharedPreferencesManager {

    public static final String USER_FOLLOWERS_KEY = "USER_FOLLOWERS_KEY";

    private static SharedPreferencesManager instance;
    private SharedPreferences sharedPreferences;


    public static SharedPreferencesManager getInstance(Context context) {
        if (instance == null) {
            instance = new SharedPreferencesManager(context);
        }
        return instance;
    }

    private SharedPreferencesManager(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveObject(String key, Object value) throws Exception {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.remove(key).apply();
        Gson gson = new Gson();
        String json = gson.toJson(value);
        prefsEditor.putString(key, json);
        prefsEditor.apply();
    }

    public <T> T getObject(String key, Class<T> classOfT) throws Exception {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, "");
        try {
            return gson.fromJson(json, classOfT);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }
}
