package eventtus.com.simpletwitterclient.activites;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eventtus.com.simpletwitterclient.R;
import eventtus.com.simpletwitterclient.adapters.UserFollowersRecyclerAdapter;
import eventtus.com.simpletwitterclient.business.AppUtils;
import eventtus.com.simpletwitterclient.business.EndlessRecyclerOnScrollListener;
import eventtus.com.simpletwitterclient.business.SharedPreferencesManager;
import eventtus.com.simpletwitterclient.business.TwitterUserFollowersServices;
import eventtus.com.simpletwitterclient.models.UserFollowersResponse;
import retrofit2.Call;


public class UserFollowersActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private UserFollowersRecyclerAdapter mAdapter;
    private RelativeLayout progressBarLayout;
    private EndlessRecyclerOnScrollListener mEndlessRecyclerOnScrollListener;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Spinner usersSpinner;

    private final int count = 10;
    private long next_cursor = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_followers);
        progressBarLayout = (RelativeLayout)findViewById(R.id.progress_bar_layout);
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.activity_main_swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        usersSpinner = (Spinner) findViewById(R.id.usersSpinner);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapter = new UserFollowersRecyclerAdapter(new ArrayList<User>());
        mRecyclerView.setAdapter(mAdapter);
        mEndlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                try {
                    retrieveFollowers(current_page);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        mRecyclerView.addOnScrollListener(mEndlessRecyclerOnScrollListener);

        retrieveFollowers(0);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                clearViews();
            }
        });

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.orange),
                getResources().getColor(R.color.green),
                getResources().getColor(R.color.blue));

        List<String> listName = new ArrayList<>();
        final List<Long> listKey = new ArrayList<>();
        for (Map.Entry<Long, TwitterSession> entry : Twitter.getSessionManager().getSessionMap().entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue().getUserName());
            listName.add(entry.getValue().getUserName());
            listKey.add(entry.getKey());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,listName); //createFromResource(this,R.array.planets_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        usersSpinner.setAdapter(adapter);
        usersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Twitter.getSessionManager().setActiveSession(Twitter.getSessionManager().getSessionMap().get(listKey.get(i)));
                clearViews();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void clearViews(){
        try {
            next_cursor = -1;
            mAdapter.clear();
            mEndlessRecyclerOnScrollListener.reset();
            mSwipeRefreshLayout.setRefreshing(false);
            retrieveFollowers(0);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void retrieveFollowers(int currentIndex){
        Log.v(getClass().getSimpleName(),"retrieveFollowers >>> index of page ="+currentIndex);
        if (AppUtils.isConnectingToNetwork(this,false)) {
            progressBarLayout.setVisibility(View.VISIBLE);
            TwitterUserFollowersServices twitterApiClient = new TwitterUserFollowersServices(Twitter.getSessionManager().getActiveSession());
            Call<UserFollowersResponse> call = twitterApiClient.getUserFollowersService().getFollowers(Twitter.getSessionManager().getActiveSession().getUserId(), count, next_cursor);
            call.enqueue(new Callback<UserFollowersResponse>() {
                @Override
                public void success(Result<UserFollowersResponse> result) {
                    progressBarLayout.setVisibility(View.GONE);
                    if (result != null && result.response.isSuccessful() && result.data != null) {
                        next_cursor = result.data.getNext_cursor();
                        mAdapter.addItems(result.data.getUsers());
                        try {
                            String userKey = SharedPreferencesManager.USER_FOLLOWERS_KEY + "_" + Twitter.getSessionManager().getActiveSession().getId();
                            SharedPreferencesManager.getInstance(UserFollowersActivity.this).saveObject(userKey, new UserFollowersResponse(mAdapter.getUsers()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        String message = "Error in retrieve data ";
                        Toast.makeText(UserFollowersActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }

                public void failure(TwitterException exception) {
                    progressBarLayout.setVisibility(View.GONE);
                    Log.v("failure >> ", exception.getMessage());
                    if (exception != null && exception.getMessage() != null) {
                        Toast.makeText(UserFollowersActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    exception.printStackTrace();
                }
            });
        } else if (currentIndex ==0){
            try {
                progressBarLayout.setVisibility(View.VISIBLE);
                String userKey = SharedPreferencesManager.USER_FOLLOWERS_KEY + "_" + Twitter.getSessionManager().getActiveSession().getId();
                UserFollowersResponse userFollowersResponse = SharedPreferencesManager.getInstance(UserFollowersActivity.this).getObject(userKey, UserFollowersResponse.class);
                if (userFollowersResponse!= null ) {
                    mAdapter.addItems(userFollowersResponse.getUsers());
                    progressBarLayout.setVisibility(View.GONE);
                } else {
                    AppUtils.showAlertDialog(this, getResources().getString(R.string.common_screen_no_internet_connection));
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}