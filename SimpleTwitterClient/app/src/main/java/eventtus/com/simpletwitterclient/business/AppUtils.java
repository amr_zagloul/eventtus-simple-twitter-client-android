package eventtus.com.simpletwitterclient.business;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import eventtus.com.simpletwitterclient.R;

/**
 * Created by amrzagloul on 10/1/16.
 */

public class AppUtils {

    public static boolean isConnectingToNetwork(Context context, boolean showMessage) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
                return true;
            }

            if (showMessage) {
                showAlertDialog(context, context.getResources().getString(R.string.common_screen_no_internet_connection));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void showAlertDialog(Context context, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false).setNegativeButton(context
                    .getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
