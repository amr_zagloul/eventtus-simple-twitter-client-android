package eventtus.com.simpletwitterclient.business;

import eventtus.com.simpletwitterclient.models.UserFollowersResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by amrzagloul on 9/30/16.
 */

public interface UserFollowersService {

    @GET("/1.1/followers/list.json")
    Call<UserFollowersResponse> getFollowers(@Query("user_id") long id, @Query("count") long count, @Query("cursor") long cursor);

}
