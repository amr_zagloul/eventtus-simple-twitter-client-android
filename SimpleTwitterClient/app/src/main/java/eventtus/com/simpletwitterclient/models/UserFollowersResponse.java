package eventtus.com.simpletwitterclient.models;

import com.twitter.sdk.android.core.models.User;

import java.util.List;

/**
 * Created by amrzagloul on 9/30/16.
 */



public class UserFollowersResponse {

    public UserFollowersResponse(){}

    public UserFollowersResponse(List<User> users){
        this.users = users;
    }

    public List<User> users;

    public long next_cursor;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public long getNext_cursor() {
        return next_cursor;
    }

    public void setNext_cursor(long next_cursor) {
        this.next_cursor = next_cursor;
    }

}
