package eventtus.com.simpletwitterclient.business;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by amrzagloul on 9/30/16.
 */

public class TwitterUserFollowersServices extends TwitterApiClient {
    public TwitterUserFollowersServices(TwitterSession session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public UserFollowersService getUserFollowersService() {
        return getService(UserFollowersService.class);
    }

}