package eventtus.com.simpletwitterclient.activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import java.util.ArrayList;
import java.util.List;

import eventtus.com.simpletwitterclient.R;
import eventtus.com.simpletwitterclient.adapters.UserTweetsRecyclerAdapter;
import retrofit2.Call;

public class UserDetailsActivity extends AppCompatActivity {

    ImageView userImage;
    ImageView backgroundImage;
    long userID = -1;
    String userName;
    String userImageUrl;
    String userBackgroundUrl;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private UserTweetsRecyclerAdapter mAdapter;
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        userImage = (ImageView) findViewById(R.id.userImage);
        backgroundImage = (ImageView) findViewById(R.id.backgroundImage);
        progressBarLayout = (RelativeLayout)findViewById(R.id.progress_bar_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapter = new UserTweetsRecyclerAdapter(new ArrayList<Tweet>());
        mRecyclerView.setAdapter(mAdapter);

        if (getIntent().getExtras()!= null ){
            userID = getIntent().getExtras().getLong("UserID");
            userName = getIntent().getExtras().getString("UserName");
            userImageUrl = getIntent().getExtras().getString("UserImage");
            userBackgroundUrl = getIntent().getExtras().getString("UserBackground");
        }

        if (userID!= -1) {
            Picasso.with(this).load(userImageUrl).into(userImage);
            if (userBackgroundUrl != null && !"".equals(userBackgroundUrl)) {
                Picasso.with(this).load(userBackgroundUrl).into(backgroundImage);
            } else {
                backgroundImage.setImageResource(R.drawable.user_default);
            }
            setTitle(userName);
            retrieveTweets();
        }
    }


    private void retrieveTweets(){

//        final UserTimeline userTimeline = new UserTimeline.Builder()
//                .userId(userID)
//                .build();
//        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(this)
//                .setTimeline(userTimeline)
//                .build();
//
//        listView.setAdapter(adapter);

        progressBarLayout.setVisibility(View.VISIBLE);

        TwitterApiClient twitterApiClient = Twitter.getInstance().getApiClient();
        StatusesService statusesService = twitterApiClient.getStatusesService();
        Call<List<Tweet>> call = statusesService.userTimeline(userID,null,10,null,null,false,true,true,false);
        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void success(Result<List<Tweet>> result) {
                progressBarLayout.setVisibility(View.GONE);

                //Do something with result
                if (result != null && result.response.isSuccessful() && result.data != null) {
                    mAdapter.addItems(result.data);
                } else {
                    String message = "Error in retrieve data ";
                    Toast.makeText(UserDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }

            public void failure(TwitterException exception) {
                progressBarLayout.setVisibility(View.GONE);
                //Do something on failure
                Toast.makeText(UserDetailsActivity.this,"error + size ="+ exception.getMessage(),Toast.LENGTH_SHORT).show();
                exception.printStackTrace();
            }
        });

    }

}
