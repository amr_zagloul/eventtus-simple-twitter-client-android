package eventtus.com.simpletwitterclient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import eventtus.com.simpletwitterclient.R;

/**
 * Created by amrzagloul on 10/1/16.
 */

public class UserTweetsRecyclerAdapter extends RecyclerView.Adapter<UserTweetsRecyclerAdapter.TweetHolder> {
    List<Tweet> tweets = new ArrayList<>();
    public UserTweetsRecyclerAdapter(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    @Override
    public TweetHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_user_tweet_row, parent, false);
        return new TweetHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(final TweetHolder holder, int position) {
        Tweet item = tweets.get(position);
        holder.bindData(item);
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }

    public void clear(){
        tweets.clear();
        notifyDataSetChanged();
    }

    public List<Tweet> getTweets(){
        return tweets;
    }

    public void addItems(List<Tweet> items){
        tweets.addAll(items);
        notifyDataSetChanged();
    }

    public static class TweetHolder extends RecyclerView.ViewHolder {
        private ImageView twTweetAuthorAvatar;
        private TextView twTweetText;
        private TextView twTweetAuthorFullName;
        private TextView twTweetAuthorScreenName;
        private View cellView;
        private Tweet tweet;

        public TweetHolder(View v) {
            super(v);
            cellView = v.findViewById(R.id.tw__tweet_view);
            twTweetAuthorAvatar = (ImageView) v.findViewById(R.id.tw__tweet_author_avatar);
            twTweetText = (TextView) v.findViewById(R.id.tw__tweet_text);
            twTweetAuthorFullName = (TextView) v.findViewById(R.id.tw__tweet_author_full_name);
            twTweetAuthorScreenName = (TextView) v.findViewById(R.id.tw__tweet_author_screen_name);
        }


        public void bindData(Tweet tweet) {
            this.tweet = tweet;
            Picasso.with(twTweetAuthorAvatar.getContext()).load(tweet.user.profileImageUrl).into(twTweetAuthorAvatar);
            twTweetText.setText(tweet.text);
            twTweetAuthorFullName.setText(tweet.user.name);
            twTweetAuthorScreenName.setText(tweet.user.screenName);
        }
    }
}
