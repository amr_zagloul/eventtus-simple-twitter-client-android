package eventtus.com.simpletwitterclient.activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterSession;

import eventtus.com.simpletwitterclient.R;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "B0U6A3YNPb87LtuZX09Vtk4tS";
    private static final String TWITTER_SECRET = "vrmsOLqZBHwwZG9pC2i0Tom8lQeB4CGitOrljxXMhrEZPvC3l6";

    private static final int WAITING_TIME = 2*1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new Twitter(authConfig));

        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();



        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(WAITING_TIME);
                } catch (Exception e){
                    e.printStackTrace();
                }

                SplashActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TwitterSession session = Twitter.getSessionManager().getActiveSession();
                        Intent intent = null;
                        if (session == null) {
                            intent = new Intent(SplashActivity.this, LoginActivity.class);
                        } else {
                            intent = new Intent(SplashActivity.this, UserFollowersActivity.class);
                        }
                        SplashActivity.this.startActivity(intent);
                        SplashActivity.this.finish();
                    }
                });

            }
        }).start();
    }
}
