package eventtus.com.simpletwitterclient.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.models.User;

import java.util.ArrayList;
import java.util.List;

import eventtus.com.simpletwitterclient.R;
import eventtus.com.simpletwitterclient.activites.UserDetailsActivity;

/**
 * Created by amrzagloul on 10/1/16.
 */

public class UserFollowersRecyclerAdapter extends RecyclerView.Adapter<UserFollowersRecyclerAdapter.UserHolder> {
    List<User> users = new ArrayList<>();
    public UserFollowersRecyclerAdapter(List<User> users) {
        this.users = users;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_user_follower_row, parent, false);
        return new UserHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(final UserHolder holder, int position) {
        User item = users.get(position);
        holder.bindData(item);
        holder.cellView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), UserDetailsActivity.class);
                if (holder.user!= null) {
                    intent.putExtra("UserID", holder.user.id);
                    intent.putExtra("UserName", holder.user.name);
                    intent.putExtra("UserImage", holder.user.profileImageUrl);
                    intent.putExtra("UserBackground", holder.user.profileBannerUrl);
                }
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void clear(){
        users.clear();
        notifyDataSetChanged();
    }

    public List<User> getUsers(){
        return users;
    }

    public void addItems(List<User> items){
        users.addAll(items);
        notifyDataSetChanged();
    }

    public static class UserHolder extends RecyclerView.ViewHolder {
        private ImageView mItemImage;
        private TextView mItemName;
        private TextView mItemBio;
        private TextView mItemHandle;
        private View cellView;
        private User user;

        public UserHolder(View v) {
            super(v);
            cellView = v.findViewById(R.id.cellView);
            mItemImage = (ImageView) v.findViewById(R.id.item_image);
            mItemName = (TextView) v.findViewById(R.id.item_name);
            mItemHandle = (TextView) v.findViewById(R.id.item_handle);
            mItemBio = (TextView) v.findViewById(R.id.item_bio);
        }


        public void bindData(User user) {
            this.user = user;
            Picasso.with(mItemImage.getContext()).load(user.profileImageUrl).into(mItemImage);
            mItemName.setText(user.name);
            mItemHandle.setText("@"+user.screenName);
            if (user.description != null && !"".equals(user.description)) {
                mItemBio.setVisibility(View.VISIBLE);
                mItemBio.setText(user.description);
            } else {
                mItemBio.setVisibility(View.GONE);
            }
        }
    }
}
